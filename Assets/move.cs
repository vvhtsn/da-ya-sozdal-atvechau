﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move : MonoBehaviour {
    float x,y;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");

        if (x>0.1f)
            transform.rotation = Quaternion.Euler(0, 90, -90);

        if (x<-0.1f)
            transform.eulerAngles = new Vector3( 180, 90, -90); 

        if (y>0.1f)
            transform.rotation = Quaternion.Euler(-90, 90, -90);

        if (y<-0.1f)
            transform.rotation = Quaternion.Euler(90, 90, -90);
    }
}
